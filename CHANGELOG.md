<a name="unreleased"></a>
## [Unreleased]


<a name="v1.2.0"></a>
## [v1.2.0] - 2019-02-09
### Feature
- **ui:** add copy by click features

### Improving application
- **ui:** now image tag are clickable


<a name="1.1.0"></a>
## [1.1.0] - 2019-02-09
### Feature
- **ui:** add footer that explain about license


<a name="1.0.0"></a>
## 1.0.0 - 2019-02-09
### Documentation
- **readme:** remove default nuxt readme

### Feature
- **page:** add normal index file
- **ui:** implement index page and add more icon

### Fixes Bug
- **fs:** fs will not work in browser

### Improving application
- **image:** change deploy docker image to lite version
- **logo:** add kcnt icon to folder


[Unreleased]: https://gitlab.com/kamontat/logo/compare/v1.2.0...HEAD
[v1.2.0]: https://gitlab.com/kamontat/logo/compare/1.1.0...v1.2.0
[1.1.0]: https://gitlab.com/kamontat/logo/compare/1.0.0...1.1.0
